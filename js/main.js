// se registra el correo y contraseña en el localstorage
// para verificar ejecutar de la siguiente forma:
// en consola del navegador web
// localStorage.setItem("correo","contraseña")
// después prueba en la vista si funciona
// coloca el correo y contraseña y clic en "Iniciar Sesión."

var nombre; //Nombre del Usuario
var apellido; //Apellido del Usuario
var contraseña; //Password del Usuario
var contraseñaConfirmada; //Password confirmado del usuario
var correo; //Correo electronico del usuario
var correotmp; //Valor temporal para almacenar correo
var fechaNacimiento; //Fecha de nacimiento del Usuario
var contraseñatmp; //Variable temporar para almacenar contraseña usuario
var restriccionCorreo = /\w+@\w+\.+[a-z]/;
var secciones = [];
var usuarioRegistrado = false;
var mascotaRegistrada = false;
var nombrerecordatorio; //Nombre del recordatorio
var fechaRecordatorio; //Fecha del recordatorio para Usuario
var number;
var namerecordatorios = [];
var daterecordatorios = [];

//Mascota
var nombreMascota;
var fechaNacimientoMascota;
var razaMascota;
var pesoMascota;


window.onload = function(){
    inicializarReferencias();
    cambiar();
    
}

function inicializarReferencias(){
    secciones[1]=   document.getElementById("splash_pantalla");
    secciones[2] = document.getElementById("pantalla_menu_principal");
    secciones[3] = document.getElementById("pantalla_registro_persona");
    secciones[4] = document.getElementById("pantalla_login");
    secciones[5] = document.getElementById("pantalla_registro_mascota");
    secciones[6] = document.getElementById("pantalla_Aseo_verde");
    secciones[7] = document.getElementById("pantalla_Salud_naranja");
    secciones[8] = document.getElementById("pantalla_Aseo_rosa");
    secciones[9] = document.getElementById("pantalla_Salud_rosa");
    secciones[10] = document.getElementById("pantalla_Recordatorio");
    secciones[11] = document.getElementById("pantalla_Menu");
    secciones[12] = document.getElementById("pantalla_editar_perfil");
    secciones[13] = document.getElementById("pantalla_fundacion");
    secciones[14] = document.getElementById("pantalla_configuracion");
    secciones[15] = document.getElementById("pantalla_perfil_mascota");

    namerecordatorios[1] = document.getElementById("subtitulo_Aseo_Mascota");
    daterecordatorios[1] = document.getElementById("fecha_Aseo_Mascota");
    namerecordatorios[2] = document.getElementById("subtitulo_Aseo_Mascota2");
    daterecordatorios[2] = document.getElementById("fecha_Aseo_Mascota2");
    namerecordatorios[3] = document.getElementById("subtitulo_Aseo_Mascota3");
    daterecordatorios[3] = document.getElementById("fecha_Aseo_Mascota3");
    namerecordatorios[4] = document.getElementById("subtitulo_Aseo_Mascota4");
    daterecordatorios[4] = document.getElementById("fecha_Aseo_Mascota4");
    namerecordatorios[5] = document.getElementById("subtitulo_Aseo_Mascota5");
    daterecordatorios[5] = document.getElementById("fecha_Aseo_Mascota5");
    namerecordatorios[6] = document.getElementById("subtitulo_Aseo_Mascota6");
    daterecordatorios[6] = document.getElementById("fecha_Aseo_Mascota6");
    namerecordatorios[7] = document.getElementById("subtitulo_Salud_Mascota");
    daterecordatorios[7] = document.getElementById("fecha_Salud_Mascota");
    namerecordatorios[8] = document.getElementById("subtitulo_Salud_Mascota2");        
    daterecordatorios[8] = document.getElementById("fecha_Salud_Mascota2");
    namerecordatorios[9] = document.getElementById("subtitulo_Salud_Mascota3");
    daterecordatorios[9] = document.getElementById("fecha_Salud_Mascota3");
    namerecordatorios[10] = document.getElementById("subtitulo_Salud_Mascota4");        
    daterecordatorios[10] = document.getElementById("fecha_Salud_Mascota4");


    
}


function cambiar(){
    secciones[1].className = "splash oculto";
    secciones[2].className = "pantalla_menu_principal";
}



function cambiarSeccion(id_seccion){

    
var i;

if((id_seccion == 6) || (id_seccion == 8)){
    number=1;
}
else if((id_seccion == 7) || (id_seccion == 9)){
    number=2;
}

for(i=1; i<=15; i++){
    secciones[i].classList.add("oculto");
}
    secciones[id_seccion].classList.add("animated");
    secciones[id_seccion].classList.add("headShake");
    secciones[id_seccion].classList.remove("oculto");

}

//REGISTRO
function registrarUsuario(){
    validarDatosUsuarioRegistrados(0);
    
}

//ACTUALIZAR USUARIO
function actualizarUsuario(){
    validarDatosUsuarioRegistrados(1);
    
}

//MASCOTA
function registrarMascota(){
    validarDatosMascotaRegistrada();
    
    
}


function validarDatosUsuarioRegistrados(number){
    if(number===0){//RegistroUsuario
        nombre = document.getElementById("nombreregistroinput").value;
        apellido = document.getElementById("apellidoregistroinput").value;
        correo = document.getElementById("correoregistroinput").value;
        contraseña = document.getElementById("contraseñainput").value;
        fechaNacimiento = document.getElementById("fechadenacimientooinput").value;
        contraseñaConfirmada = document.getElementById("contraseñaconfirmadaregistroinput").value;
        if(contraseñaConfirmada=== ""){
            Swal.fire({
                icon: 'error',
                html: '<p style="font-family:Roboto-bold" >Todos los campos son obligatorios</p>'
            })
        }
        else if(contraseña !== contraseñaConfirmada){
            Swal.fire({
                icon: 'error',
                html: '<p style="font-family:Roboto-bold" >Las contraseñas no coinciden</p>'
            })
        }
    }
    else{//Actualizar Usuario
        nombre = document.getElementById("nombreregistroinput_act").value;
        apellido = document.getElementById("apellidoregistroinput_act").value;
        correo = document.getElementById("correoregistroinput_act").value;
        contraseña = document.getElementById("contraseñainput_act").value;
        fechaNacimiento = document.getElementById("fechadenacimientooinput_act").value;
    }


    if( (nombre === "") || (apellido === "") || (correo === "") || (contraseña === "") || (fechaNacimiento === "")  ){
        Swal.fire({
            icon: 'error',
            html: '<p style="font-family:Roboto-bold" >Todos los campos son obligatorios</p>'
        })
    }
    else if(!restriccionCorreo.test(correo)){ 
        Swal.fire({
            icon: 'error',
            html: '<p style="font-family:Roboto-bold" >El correo no es valido</p>'
        })
    }
    else if(contraseña !== contraseñaConfirmada){
        Swal.fire({
            icon: 'error',
            html: '<p style="font-family:Roboto-bold" >Las contraseñas no coinciden</p>'
        })
    }
    else{
        almacenarDatosEnLocalStorage();
        Swal.fire({
            icon: 'success',
            html: '<p style="font-family:Roboto-bold" >Registro exitoso</p>'
        })
        cambiarSeccion(5); 
    }


}

function almacenarDatosEnLocalStorage(){
    localStorage.setItem('nombreUsuario',nombre);
    localStorage.setItem('apellidoUsuario',apellido);
    localStorage.setItem('correoUsuario',correo);
    localStorage.setItem('contraseñaUsuario',contraseña);
    localStorage.setItem('fechaNacimiento',fechaNacimiento);
    document.getElementById("correorecordatorioinput").value = localStorage.getItem('correoUsuario');
    document.getElementById("nombrepersonarecordatorioinput").value = localStorage.getItem('nombreUsuario',nombre);
    

}

function buscarCorreo(){
    correo = document.getElementById("correologininput").value;
    contraseña = document.getElementById("contraseñalogininput").value;
    if(localStorage.getItem('correoUsuario') != null) {
        correotmp = localStorage.getItem('correoUsuario');
    } else {
         // alert
         Swal.fire({
            icon: 'error',
            html: '<p style="font-family:Roboto-bold" >No hay correos registrados</p>'
          })
    }

    if(localStorage.getItem('contraseñaUsuario') != null) {
        contraseñatmp = localStorage.getItem('contraseñaUsuario');
    } else {
         // alert
         Swal.fire({
            icon: 'error',
            html: '<p style="font-family:Roboto-bold" >No hay contraseñas registradas</p>'
          })
    }
}

function validar(){
    buscarCorreo();

    if( (correo === correotmp) && (contraseña === contraseñatmp)){
        //alert
        Swal.fire({
            icon: 'success',
            html: '<p style="font-family:Roboto-bold" >Inicio de sesión exitoso</p>',
            showConfirmButton: false,
            timer: 1500
          })
        cambiarSeccion(15);
    }
    else if(correo !== correotmp){
        //alert
        Swal.fire({
            icon: 'error',
            html: '<p style="font-family:Roboto-bold" >Correo no valido</p>'
          })
    }
    else if(contraseña !== contraseñatmp){
        //alert
        Swal.fire({
            icon: 'error',
            html: '<p style="font-family:Roboto-bold" >Contraseña erronea</p>'
          })
    }


}

function validarDatosMascotaRegistrada(){
    nombreMascota = document.getElementById("nombremascotaininput").value;
    fechaNacimientoMascota = document.getElementById("fechadenacimientoinput").value;
    razaMascota = document.getElementById("razainput").value;
    pesoMascota = document.getElementById("pesoinput").value;
    

    if( (nombreMascota === "") || (fechaNacimientoMascota === "") || (razaMascota === "") || (pesoMascota === "") ){
        //alert
        Swal.fire({
            icon: 'error',
            html: '<p style="font-family:Roboto-bold" >Todos los campos son obligatorios</p>'
        })
    }
    else if(pesoMascota<=0){ 
        //alert
        Swal.fire({
            icon: 'error',
            html: '<p style="font-family:Roboto-bold" >Peso de la mascota no válido</p>'
        })
    }
    else{
        almacenarDatosMascotaEnLocalStorage();
        //alert
        Swal.fire({
            icon: 'success',
            html: '<p style="font-family:Roboto-bold" >Registro mascota exitoso</p>',
            showConfirmButton: false,
            timer: 1500
        })

        

        cambiarSeccion(8);
        mascotaRegistrada = true;

        





    }
}

function almacenarDatosMascotaEnLocalStorage(){
    localStorage.setItem('nombreMascota',nombreMascota);
    localStorage.setItem('fechaNacimientoMascota',fechaNacimientoMascota);
    localStorage.setItem('razaMascota',razaMascota);
    localStorage.setItem('pesoMascota',pesoMascota);

    document.getElementById("titulomascotainput").value = localStorage.getItem('nombreMascota');
    document.getElementById("pesomascotainput").value = localStorage.getItem('pesoMascota');


    document.getElementById("nombreperrorecordatorioinput").value = localStorage.getItem('nombreMascota');
}

function programarRecordatorio(){

    nombrerecordatorio = document.getElementById("nombrerecordatorioinput").value;
    fechaRecordatorio = document.getElementById("fecharecordatorioinput").value;
    var i;

    if( (nombrerecordatorio === "") || (fechaRecordatorio === "")  ){
             //alert
        Swal.fire({
            icon: 'error',
            html: '<p style="font-family:Roboto-bold" >Todos los campos son obligatorios</p>'
        })
    }
    else{ 
        if(number == 1){
            for(i=1; i<=6;i++){
                namerecordatorios[i].innerHTML = nombrerecordatorio;
                daterecordatorios[i].innerHTML = fechaRecordatorio;

            }

        }
        
        else if(number == 2){
            for(i=7; i<=10;i++){
                namerecordatorios[i].innerHTML = nombrerecordatorio;
                daterecordatorios[i].innerHTML = fechaRecordatorio;

            }
        }
        
    cambiarSeccion(15);
    }  
}

function enviarCorreo(){

    (function(){
        emailjs.init("user_DzwMqiEVr6n8sqL6KCVq5");
    })();
    
    var myform = $("form#myform");
    myform.submit(function(event){
        event.preventDefault();
    
        var params = myform.serializeArray().reduce(function(obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    
      // Change to your service ID, or keep using the default service
    var service_id = "gmail";
    
    var template_id = "correo_app";
    emailjs.send(service_id, template_id, params)
        .then(function(){ 
            Swal.fire({
                icon: 'success',
                html: '<p style="font-family:Roboto-bold" >Recordatorio enviado</p>',
            })
        }, function(err) {
            Swal.fire({
                icon: 'error',
                html: '<p style="font-family:Roboto-bold" >No se pudo enviar el recordatorio</p>'
            })
            
        });
    
        return false;
    });   
}

